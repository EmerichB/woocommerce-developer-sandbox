# README #
Dieses �ffentliche Repository ist eine WordPress/Woocommerce Installation speziell zum Kennenlernen unserer Workflows.

Jedes Kundenprojekt befindet sich in einem privaten Bitbucket Repository. �ber deinen Bitbucket Benutzer erh�ltst du Schreibzugriff auf deine Projekte.

Zentraler Einstiegspunkt f�r EntwicklerInnen ist das Bitbucket Wiki, in dem wichtige Informationen zum Projekt zu finden sind und andere Dokumente wie Endkundendokumentation verlinkt sind.

Deine �nderungen committest du ausschliesslich in einen feature Branch, welcher von Lead DeployerIn (oder dessen stellvertr. DeployerIn) des jeweiligen Projekts zuerst in den develop Branch und abschliessend in den master Branch gemergt und auf einer Test- bzw. Live-Umgebung deployed wird.

Seit  Anfang 2018 setzen wir einen einheitlichen [Git Feature Branch Workflow](https://docs.openstream.ch/team/git-workflow/) f�r alle WordPress, WooCommerce und Magento Projekte ein.